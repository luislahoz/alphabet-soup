package org.example.controller;

import org.example.model.AlphabetSoupModel;
import org.javatuples.Pair;

public class AlphabetSoupController {

    private AlphabetSoupModel model;
    private String output;

    private enum Directions{
        toSouth,
        toNorth,
        toEast,
        toWest,
        toSouthWest,
        toNorthWest,
        toSouthEast,
        toNorthEast
    }

    public AlphabetSoupController(String word, int rowCount, int columnCount, char[][] grid){

        this.output = null;

        // Find first letter match then look around to see to 
        // what direction we can find the word
        char[] wordArray = word.toCharArray();
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                if(grid[i][j] == wordArray[0]){
                    Pair<Integer,Integer> firstLocation = new Pair<Integer, Integer> (Integer.valueOf(i), Integer.valueOf(j));
                    this.model = new AlphabetSoupModel(word, firstLocation);
                    this.model.setLastLocation(lookAround(grid, word.toCharArray(), i, j));
                    if (this.model.getLastLocation() != null){
                        this.output = this.model.toString();
                    }
                }
            }
        }
    }

    public String getOutput(){
        return this.output;
    }

   // Look around the first letter found, if second letter found keep diggin in that direction
   public static Pair <Integer, Integer> lookAround(char[][] grid, char[] wordArray, int row, int column){

    // Pair <Integer, Integer> lastPosition = new Pair<Integer, Integer> (null, null);
    int[] lastPosition;

    // Hold if going in that direction is possible
    boolean goSouth = (row+wordArray.length-1) < grid[0].length;
    boolean goNorth = (row-(wordArray.length-1)) >= 0;
    boolean goEast = (column+wordArray.length-1) < grid.length;
    boolean goWest = (column-(wordArray.length-1)) >= 0;

    //To SOUTH
    if (goSouth){
        if((lastPosition = toCoordinates(grid, wordArray, row+1, column, 1, Directions.toSouth)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]-1), Integer.valueOf(lastPosition[1]));
        }
    }
    //To North
    if (goNorth){
        if((lastPosition = toCoordinates(grid, wordArray, row-1, column, 1, Directions.toNorth)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]+1), Integer.valueOf(lastPosition[1]));
        }
    }
    // To EAST
    if(goEast){
        if((lastPosition = toCoordinates(grid, wordArray, row, column+1, 1, Directions.toEast)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]), Integer.valueOf(lastPosition[1]-1));
        }
    }
    //To WEST
    if(goWest){
        if((lastPosition = toCoordinates(grid, wordArray, row, column-1, 1, Directions.toWest)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]), Integer.valueOf(lastPosition[1]+1));
        }
    }
    //To SouthWest
    if (goSouth && goWest){
        if((lastPosition = toCoordinates(grid, wordArray, row+1, column-1, 1, Directions.toSouthWest)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]-1), Integer.valueOf(lastPosition[1]+1));
        }
    }
    //To NorthWest
    if (goNorth && goWest){
        if((lastPosition = toCoordinates(grid, wordArray, row-1, column-1, 1, Directions.toNorthWest)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]+1), Integer.valueOf(lastPosition[1]+1));
        }
    }
    //To SouthEast
    if (goSouth && goEast){
        if((lastPosition = toCoordinates(grid, wordArray, row+1, column+1, 1, Directions.toSouthEast)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]-1), Integer.valueOf(lastPosition[1]-1));
        }
    }
    //To NorthEast        
    if (goNorth && goEast){
        if((lastPosition = toCoordinates(grid, wordArray, row-1, column+1, 1, Directions.toNorthEast)) != null){
            return Pair.with(Integer.valueOf(lastPosition[0]+1), Integer.valueOf(lastPosition[1]-1));
        }
    }

    return null;
}

    private static int[] toCoordinates(char[][] grid, char[] wordArray, int rowNextLetter, int columnNextLetter, int wordArrayPosition, Directions direction){

        //Done looking recursivelly, the word is has been found
        if (wordArrayPosition == wordArray.length){
            int[] lastPosition = {rowNextLetter, columnNextLetter};
            return lastPosition;
        }
        //Keep the values within limit
        if (rowNextLetter < 0 || columnNextLetter < 0 || rowNextLetter > grid.length || columnNextLetter > grid[0].length){
            return null;
        }

        // Recursive call 
        if (grid[rowNextLetter][columnNextLetter] == wordArray[wordArrayPosition]){
            switch(direction){
                case toSouth:
                    return toCoordinates(grid, wordArray, rowNextLetter+1, columnNextLetter, wordArrayPosition+1, Directions.toSouth);
                case toNorth:
                    return toCoordinates(grid, wordArray, rowNextLetter-1, columnNextLetter, wordArrayPosition+1, Directions.toNorth);
                case toEast:
                    return toCoordinates(grid, wordArray, rowNextLetter, columnNextLetter+1, wordArrayPosition+1, Directions.toEast);
                case toWest:
                    return toCoordinates(grid, wordArray, rowNextLetter, columnNextLetter-1, wordArrayPosition+1, Directions.toWest);
                case toSouthWest:
                    return toCoordinates(grid, wordArray, rowNextLetter+1, columnNextLetter-1, wordArrayPosition+1, Directions.toSouthWest);
                case toNorthWest:
                    return toCoordinates(grid, wordArray, rowNextLetter-1, columnNextLetter-1, wordArrayPosition+1, Directions.toNorthWest);
                case toSouthEast:
                    return toCoordinates(grid, wordArray, rowNextLetter+1, columnNextLetter+1, wordArrayPosition+1, Directions.toSouthEast);
                case toNorthEast:
                    return toCoordinates(grid, wordArray, rowNextLetter-1, columnNextLetter+1, wordArrayPosition+1, Directions.toNorthEast);
                default:
                    break;
            }
        }
        return null;
    }

}
