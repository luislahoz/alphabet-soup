package org.example;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.example.controller.AlphabetSoupController;

public class AlphabetSoup {

    public static void main(String[] args) throws IOException {

        System.out.println("Input file location and filename (ex. /opt/dataSample.txt)");

        // Open pathFile from user input
        Scanner scanner = new Scanner(System.in);
        FileInputStream fstream = new FileInputStream(scanner.nextLine());
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        // Row x Column
        char[] rowsAndColumns = br.readLine().toCharArray();
        int rowCount = Character.getNumericValue(rowsAndColumns[0]);
        int columnCount = Character.getNumericValue(rowsAndColumns[2]);;

        // Matrix that will hold grid of characters
        char[][] grid = new char[rowCount][columnCount];

        // Loops to populate matrix
        for (int i = 0; i < rowCount; i++) {
            String rowItems = br.readLine().replaceAll("\\s", "");
            char[] rowArray = rowItems.toCharArray();

            for (int j = 0; j < columnCount; j++) {
                // Populate matrix from file
                grid[i][j] = rowArray[j];
            }
        }

        // List of words to find in Matrix/Grid
        ArrayList <String> wordsToFind = new ArrayList<String>();
        String newWord;
        while((newWord = br.readLine()) != null){
            wordsToFind.add(newWord.replaceAll("\\s", ""));
        }

        br.close();
        fstream.close();
        scanner.close();

        // At this point everything should be in the right place
        // Send each word to the controller to find it and print it
        for (String word: wordsToFind){
            String output = new AlphabetSoupController(word, columnCount, columnCount, grid).getOutput();
            if(output != null){
                System.out.println(output);
            }
        }
    }

}
