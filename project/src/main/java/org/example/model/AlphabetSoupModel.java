package org.example.model;

import org.javatuples.Pair;

public class AlphabetSoupModel {
    private String word;
    private Pair<Integer, Integer> firstLocation;
    private Pair<Integer, Integer> lastLocation;

    public AlphabetSoupModel(String word, Pair<Integer, Integer> firstLocation){
        this.word = word;
        this.firstLocation = firstLocation;
        this.lastLocation = null;
    }

    public String getWord(){
        return this.word;
    }

    public char[] getWordChar(){
        return this.word.toCharArray();
    }

    public Pair<Integer, Integer> getFirstLocation(){
        return this.firstLocation;
    }

    public Pair<Integer, Integer> getLastLocation(){
        return this.lastLocation;
    }

    public void setLastLocation(Pair<Integer, Integer> lastLocation){
        this.lastLocation = lastLocation;
    }

    public String toString(){
        return (word + " " + firstLocation.getValue0() + ":" + firstLocation.getValue1() + " " + lastLocation.getValue0() + ":" + lastLocation.getValue1());
    }
}
