package org.example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.example.controller.AlphabetSoupController;
import org.javatuples.Pair;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AlphabetSoupTest 
{
    private char[][] grid = {{'C','O','V','I','D','A','A','A'},
                             {'K','E','E','R','C','A','A','E'},
                             {'I','A','N','A','A','T','Y','N'},
                             {'T','A','A','A','E','A','M','I'},
                             {'C','A','P','E','I','A','A','N'},
                             {'H','A','N','M','E','R','A','A'},
                             {'E','A','A','T','A','A','D','A'},
                             {'N','A','A','A','A','L','A','A'}
                            };

    private Pair<Integer, Integer> lastLocation;

    @Test
    public void toEastTest(){
        lastLocation = new Pair<Integer, Integer> (0,4);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "COVID".toCharArray(), 0, 0), "EAST");
    }

    @Test
    public void toWestTest(){
        lastLocation = new Pair<Integer, Integer> (1,0);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "CREEK".toCharArray(), 1, 4), "WEST");
    }

    @Test
    public void toSouthTest(){
        lastLocation = new Pair<Integer, Integer> (7,0);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "KITCHEN".toCharArray(), 1, 0), "SOUTH");
    }

    @Test
    public void toNorthTest(){
        lastLocation = new Pair<Integer, Integer> (1,7);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "NINE".toCharArray(), 4, 7), "NORTH");
    }

    @Test
    public void toSouthEastTest(){
        lastLocation = new Pair<Integer, Integer> (2,6);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "DAY".toCharArray(), 0, 4), "SOUTH EAST");
    }

    @Test
    public void toSouthWestTest(){
        lastLocation = new Pair<Integer, Integer> (5,2);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "TEEN".toCharArray(), 2, 5), "SOUTH WEST");
    }

    @Test
    public void toNorthEastTest(){
        lastLocation = new Pair<Integer, Integer> (4,3);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "NANE".toCharArray(), 7, 0), "NORTH EAST");
    }

    @Test
    public void toNorthWestTest(){
        lastLocation = new Pair<Integer, Integer> (2,2);
        assertEquals(lastLocation, AlphabetSoupController.lookAround(grid, "ADRIAN".toCharArray(), 7, 7), "NORTH WEST");
    }

}
